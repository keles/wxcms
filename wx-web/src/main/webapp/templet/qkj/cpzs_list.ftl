<div class="layui-row">
    <div class="layui-col-md12 layui-col-space15">
        <#if articlelist ? exists>
            <#list articlelist as article>
                <#if article_index lte 3>
                <li class="layui-col-md3">
                    <div class="fly-panel">
                        <#if article.article_url ? exists>
                            <a class="fly-case-img" href="${article.article_url}" target="_blank">
                                <img src="${article.article_titleimg}" alt="${article.article_title}">
                                <cite class="layui-btn layui-btn-primary layui-btn-small">去围观</cite>
                            </a>
                        <#else >
                            <a class="fly-case-img" href="${channel.channel_catalog+article.article_pk+".html"}" target="_blank">
                                <img src="${article.article_titleimg}" alt="${article.article_title}">
                                <cite class="layui-btn layui-btn-primary layui-btn-small">去围观</cite>
                            </a>
                        </#if>
                        <h2>
                            <#if article.article_url ? exists>
                                <p style="text-align: center"><a href="${article.article_url}" target="_blank">${article.article_title}</a></p>
                                <#else >
                                    <p style="text-align: center"><a href="${channel.channel_catalog+article.article_pk+".html"}" target="_blank">${article.article_title}</a></p>
                            </#if>
                        </h2>
                        <#--<p class="fly-case-desc">${article.article_title}</p>-->
                    </div>
                </li>
                </#if>
            </#list>
        </#if>
    </div>
</div>

<div class="layui-row">
    <div class="layui-col-md12 layui-col-space15">
    <#if articlelist ? exists>
        <#list articlelist as article>
            <#if article_index gte 4>
                <li class="layui-col-md3">
                    <div class="fly-panel">
                        <#if article.article_url ? exists>
                            <a class="fly-case-img" href="${article.article_url}" target="_blank">
                                <img src="${article.article_titleimg}" alt="${article.article_title}">
                                <cite class="layui-btn layui-btn-primary layui-btn-small">去围观</cite>
                            </a>
                        <#else >
                            <a class="fly-case-img" href="${channel.channel_catalog+article.article_pk+".html"}" target="_blank">
                                <img src="${article.article_titleimg}" alt="${article.article_title}">
                                <cite class="layui-btn layui-btn-primary layui-btn-small">去围观</cite>
                            </a>
                        </#if>
                        <h2>
                            <#if article.article_url ? exists>
                                <p style="text-align: center"><a href="${article.article_url}" target="_blank">${article.article_title}</a></p>
                            <#else >
                                <p style="text-align: center"><a href="${channel.channel_catalog+article.article_pk+".html"}" target="_blank">${article.article_title}</a></p>
                            </#if>
                        </h2>
                    <#--<p class="fly-case-desc">${article.article_title}</p>-->
                    </div>
                </li>
            </#if>
            <#if article_index gte 7>
                <#break>
            </#if>
        </#list>
    </#if>
    </div>
</div>